<?php

/**
 * Implements methods for base routes.
 */

namespace Drupal\school\Controller;

class SchoolController {

  public function viewTest() {
    return [
      '#markup' => 'Hello, Drupal School',
    ];
  }
}
